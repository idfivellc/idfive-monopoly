0.1.12 - 20190128
=================
- Updates silc-nav to 1.0.6

0.1.11 - 20190128
=================
- Updates silc-nav to 1.0.5

0.1.10 - 20190128
=================
- Updates webpack-dev-server to 3.1.11
- Updates vue to 2.5.22
- Updates silc-nav to 1.0.4

0.1.9 - 20191701
================
- Replace uglifyjs-webpack-plugin with terser-webpackplugin for ES6 support: https://github.com/mishoo/UglifyJS2/issues/659#issuecomment-447820525

0.1.8 - 20191401
================
- Adds focus-within polyfill for IE11

0.1.7 - 20181109
================
- Update webpack config to correctly parse images in CSS
- Small code adjustments

0.1.6 - 20181101
================
- Vue component styles now get added to index.css build file

0.1.5 - 20181024
================
- Adds new entry for print styles
- Adds minification for CSS

0.1.4 - 20180924
================
- Update to silc-nav 1.0.3

0.1.3 - 20180924
================
- Update to silc-nav 1.0.1

0.1.2 - 20180920
================
- Pass arguments from npm scrips to library scripts

0.1.1 - 20180914
================
- Adds default margin/padding for paragraphs and lists
- Updates to card to allow main cta link and background images
- Adds font-weight and text-transform variables to button
- Adds iframe component
- Updates silc-utilities to 0.3.2

0.1.0 - 20180914
================
- Initial release