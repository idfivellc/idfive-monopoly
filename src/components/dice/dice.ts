
class Dice {
    protected side;
    protected angX;
    protected angY;
    protected ang = 0;
    constructor (){
        this.side = this.side;
        this.angX = this.angX;
        this.angY = this.angY;
        this.ang = this.ang;
    }
    rollSide(element) {
        this.side = 2;
        switch(this.side) {
            case 1: 
                this.angX = -90;
                this.angY = 0;
                this.ang = this.angX + this.angY;
            break;
            case 2:
                this.angX = 0;
                this.angY = 90;
                this.ang = this.angX + this.angY;
            break;
            case 3: 
                this.angX = 180;
                this.angY = 0;
                this.ang = this.angX + this.angY;
            break;
            case 4:
                this.angX = 0;
                this.angY = 270;
                this.ang = this.angX + this.angY;
            break;
            case 5:
                this.angX = 0;
                this.angY = 0;
                this.ang = this.angX + this.angY;
            break;
            case 6:
                this.angX = 90;
                this.angY = 0;
                this.ang = this.angX + this.angY;
            break;
        }
        roll(element, this.angX, this.angY, this.ang);
    };
    getSide() {
        return this.side;
    }
}

function getRandom(min, max) {
    return Math.floor(Math.random() * Math.floor(max - min) + min);
}

function roll(element, angX, angY, ang) {
    let angXFactor = angX == 0 ? 0 : angX / angX;
    let angYFactor = angY == 0 ? 0 : angY / angY;
    let transform = "rotate3d(" + angXFactor + ", " + angYFactor + ", " + 0 + ", " + ang+"deg" + ")";
    element.style.transform = transform;
}

export default Dice;