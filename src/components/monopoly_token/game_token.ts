import Token from '../monopoly_token/token'

let stepCount: number = 0;
let slotColors= [];
let slotPosX = [];
let slotPosY = [];
let widthFactor: number = 0;
let heightFactor: number = 0;
let residual: number = 0;

class GameToken {
    protected gameToken;
    protected slots;
    protected board;
    protected token;
    protected posX;
    protected posY;
    constructor(gameToken, slots, board) {
        this.gameToken = gameToken;
        this.slots = slots;
        this.board = board;
        this.token = new Token(this.gameToken, 0, 0, 0, this.posX, this.posY, stepCount);  
    }
    startGame() {
        this.getPositions();
    }
    startToken(diceSide) {
        this.token.mutateColor();
        if(stepCount + diceSide <= 40) {
            stepCount += diceSide;
        } else {
            residual = (stepCount - 40) + diceSide;
            stepCount = residual;
        }
        this.moveToken();
        this.getAxis(this.slots)
        this.getColors();
    } 
    getPositions() {
        let rect;
        let gameboardRect = this.board.getBoundingClientRect();
        for (let i = 0; i < this.slots.length; i++) {
            let slot = this.slots[i] as HTMLElement;
            if (this.slots[i].getElementsByClassName('color-shape').length > 0){
                slotColors.push(this.slots[i].querySelector('.color-shape'));
            }
            rect = slot.getBoundingClientRect();
            slotPosY[i] = rect.top - gameboardRect.top;
            slotPosX[i] = rect.left - gameboardRect.left;
        }
    }
    getColors() {
        for (let i = 0; i < this.slots.length; i++) {
            let slot = this.slots[i] as HTMLElement;
            if (this.slots[i].getElementsByClassName('color-shape').length > 0){
                slotColors.push(this.slots[i].querySelector('.color-shape'));
            }
        }
        console.log(document.defaultView.getComputedStyle(slotColors[10], null)['background-color']);
    }
    moveToken() {
        this.posX = slotPosX[stepCount];
        this.posY = slotPosY[stepCount];
        if (stepCount != 40) {
            setTimeout (() => {
                this.token.element.style.left = this.posX + widthFactor + "px";
                this.token.element.style.top = this.posY + heightFactor + "px";
            }, 100)
        } else {
            setTimeout (() => {
                this.token.element.style.left = 97 + "%";
                this.token.element.style.top = 97 + "%";
            }, 100)
        }
    }
    scaleSize(tokenSize) {
        this.token.element.style.width = tokenSize + "px";
        this.token.element.style.height = tokenSize + "px";
        this.token.element.style.transform = 'translate(-50%, -50%)';
    }
    getAxis(slots) {
        if (stepCount <= 10) {
            widthFactor = slots[stepCount].clientHeight/2
            heightFactor = slots[stepCount].clientWidth/1.5;
            if (stepCount == 10) {
                widthFactor = slots[stepCount].clientWidth / 2;
                heightFactor = slots[stepCount].clientHeight / 2;
            }
        } else if (stepCount > 10 && stepCount < 20) {
            widthFactor = slots[stepCount].clientWidth / 3.75;
            heightFactor = slots[stepCount].clientHeight / 2;
        } else if (stepCount == 20) {
            widthFactor = slots[stepCount].clientWidth / 2;
            heightFactor = slots[stepCount].clientWidth / 2;
        } else if (stepCount > 20 && stepCount < 30) {
            widthFactor = slots[stepCount].clientWidth / 4;
            heightFactor = slots[stepCount].clientHeight/2;
        } else if (stepCount == 30) {
            widthFactor = slots[stepCount].clientWidth/2;
            heightFactor = slots[stepCount].clientHeight/2;
        } else if (stepCount > 30 && stepCount < 40) {
            widthFactor = slots[stepCount].clientWidth/1.25;
            heightFactor = slots[stepCount].clientHeight/2;
        } else if (stepCount == 40) {
            console.log(stepCount, slots[0].clientWidth/2, slots[0].clientHeight/2);
            widthFactor = slots[1].clientWidth/2;
            heightFactor = slots[1].clientHeight/2;
        }
    }
    restartGame() {
        stepCount = 0 + residual;
    }
}

// function posXstep(diceSide) {
//     if (posY < 11) {
//         if (posX + diceSide <= 10) {
//             posX += diceSide;
//         } else {
//             posX += diceSide - Math.abs(residual) + 1;
//             if (posX == 11) {
//                 posX += 1;
//             }
//             posY += Math.abs(residual) + 1;
//             changeDirection();
//         }
//     } else {
//         if (posX - diceSide > 0) {
//             posX -= diceSide;
//             if (posX == 1) {
//                 posX -= 0;
//             }
//         } else {
//             posX -= diceSide - Math.abs(residual) - 1;
//             posY -= Math.abs(residual);
//             changeDirection();
//         }
//     }
//     console.log("posX:" + posX);
//     console.log("posY:" + posY);
// }

// function posYstep(diceSide) {
//     if (posY + diceSide <= 10 && posX > 0) {
//         posY += diceSide;
//     } else if (posY - diceSide > 0 && posX <= 1) {
//         posY -= diceSide;
//     } 
//      else {
//         posY += diceSide - Math.abs(residual) + 1;
//         posX -= Math.abs(residual) + 1;
//         changeDirection();
//     }
// }

// function tokenInCorner(diceSide) {
//     //when token is in corner X Axis
//     if (posX == 0) {
//         if (posY > 10) {
//             posX -= 1;
//             console.log("hey, im here");
//         } else {
//             posX += 1;
//         }
//     }
//     if (posX == 1 && posY == 12) {
//         //posX -= 1;
//         posY -= 1;
//     }
// }

// function changeDirection() {
//     allowXStep = !allowXStep;
// }

// function isCorner() {
//     isCorner;
// }

// function calcStep(stepIteration) {
//     let stepSpace = 1 / 13 * 100;
//     let initialPositionToken = document.getElementById("game__initial-position");
//     let initialPosition = initialPositionToken.offsetLeft;
//     let gameBoard = document.getElementById("main__square-id");
//     let step;
//     if (stepCount != 0) {
//         step = (((initialPosition * 100) / gameBoard.clientHeight) - ((stepSpace)*(stepIteration) + (stepCount == 40 ? 0 : 1))) + stepSpace + "%";
//     } 
//     return step;
// }

export default GameToken;