class Token {
    protected posX;
    protected posY;
    protected element;
    protected r; 
    protected g; 
    protected b;
    protected stepCount;
    constructor(element, r, g, b, posX, posY, stepCount) {
        this.posX = posX;
        this.posY = posY;
        this.element = element;
        this.r = r;
        this.g = g;
        this.b = b;
        this.stepCount = stepCount
    }
    mutateColor() {
        let {element, r, g, b, stepCount} = this;
        if (stepCount == 0) {
            r = this.randomColor();
            g = this.randomColor();
            b = this.randomColor();
        } else if (stepCount >= 10) {
            r = this.randomColor();
            g = this.randomColor();
            b = this.randomColor();
        } else if (stepCount >= 20) {
            r = this.randomColor();
            g = this.randomColor();
            b = this.randomColor();
        } else if (stepCount >=30) {
            r = this.randomColor();
            g = this.randomColor();
            b = this.randomColor();
        }
        element.style.background = 'rgb(' + r + ', ' + g + ', ' + b + ')'
    }
    randomColor() {
        return Math.floor(Math.random() * Math.floor(255 - 0) + 0);
    }
}

export default Token;