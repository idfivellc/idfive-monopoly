
import Dice from '../dice/dice';
import GameToken from '../monopoly_token/game_token';

function World() {
    let board = document.getElementById("main__square-id");
    let diceElement = document.getElementById('dice_id');
    let tokenElement = document.getElementById('game-token');
    let slots = document.getElementsByClassName('step-space');
    let dice = new Dice();
    let token = new GameToken(tokenElement, slots, board);
    token.startGame();
    window.addEventListener('resize', () => {
        setTimeout (() => {
            token.getPositions();
            token.moveToken();
        }, 1000)
        token.scaleSize(board.clientWidth / 15);
        token.getAxis(slots);
    });
    let advanceButton = document.getElementById('advance-btn');
    advanceButton.addEventListener('click', () => {
        dice.rollSide(diceElement)
        setTimeout(() => {
            token.startToken(dice.getSide());
        }, 1000)
    });
}

export default World;