/**
 * Modules
 */
import { silcCoreInit } from "silc-core";
import { silcAccordionInit } from "silc-accordion";
import { silcNavInit } from "silc-nav";
import { silcOffcanvasInit } from "silc-offcanvas";
import focusWithin from "focus-within";

import Flickity from "../components/carousel/carousel";
import Tablesaw from "../components/table/table";

/*
* My own resizing function
*/
import oneStepAhead from "../components/monopoly/monopoly";
import Dice from '../components/dice/dice'
import World from "../components/monopoly/monopoly";

/**
 * Init
 */
focusWithin(document);
silcCoreInit();
silcAccordionInit();
silcNavInit();
silcOffcanvasInit();
let world = new World();
Flickity;
Tablesaw.init();
